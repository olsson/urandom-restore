# urandom-restore

If a file has been backed up to **/dev/null** there is a chance that
you might be able to restore it from **/dev/urandom**. That is what
_urandom-restore_ is for.


## Usage

_urandom-restore_ is called with the name of the file you want to
restore as its one argument.

    $ ./urandom-restore taxes2016.ods
    /tmp/tmpiy7xg93r/taxes2016.ods

To test backing up and restoring a file there is the included shell
script _test-backup-and-restore_.

    $ ./test-backup-and-restore ~/Documents/taxes2017.ods
    The original /home/andreas/Documents/taxes2017.ods does not match the restored /tmp/tmppxr16hsu/taxes2017.ods


## Limitations

The free version of _urandom-restore_ only supports restoring files
which are 50 MiB or smaller.
